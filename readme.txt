=== Book Carousel ===
Contributors: jakeparis
Donate link: https://jakeparis.com
Requires at least: 4.6
Tested up to: 5.6.2
Stable tag: 5.0.1
Requires PHP: 5.5

Adds functionality for a Book Carousel which links directly to a library catalog, or anywhere really.


== Description ==

This plugin add the ability to create a carousel of book covers with customizable links. The links can be set up to point, for example, at a library catalog. The source of the cover images is also customizable.

== Changelog ==

= 5.2.1 =

* No user-facing changes
* Version bump; changed location of source code. 

= 5.1.0 =
Added alt tags for better accessibility compliance. Thanks https://betteraccesspro.com

= 5.0.1 =

* Fixed bug where plugin wouldn't properly activate if certain other plugins
  were in use. 

= 5.0.0 =
Added requirement for license keys. After performing this update, please contact me for a license key.

* Updated vendor library for plugin updates to latest.

= 4.4.2 =
Minor behind the scenes update.

= 4.4.1 =
Updated update mechanism. Oh so meta.

= 4.4.0 = 
* Added an updater

= 4.3.0 =
* Added caching of the images for *much* faster loading times.

= 4.2.1 =
* Added backwards compatibility with < PHP5.4

= 4.2.0 =
* Re-architected the plugin. Moved assets into assets dir. Cleaned up the code.

= 4.1 =
* Removed some custom sql in favor of standard WP functions
