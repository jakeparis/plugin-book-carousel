jQuery(function($){
	// ajax the book covers
	if(typeof book_carousel_obj === 'undefined') 
		return;

	var carousel = document.getElementById('bookCarousel');

	if( ! carousel )
		return;

	carousel.classList.add('bookCarouselLoading');

	var isbns = book_carousel_obj.isbns;
	for(var i=0; i<isbns.length; i++){
		$.post( book_carousel_obj.ajaxurl, {
			action: 'getBookCover',
			isbn: isbns[i],
		}, function(resp){
			// after we've loaded 5, we can remove the loading spinner, even though
			// more may be loading. Or if this is the last one
			var filler = carousel.querySelector('#bookFiller');
			if( filler )
				filler.parentNode.removeChild(filler);

			var soFar = carousel.querySelectorAll('img').length + 1;
			if( soFar > 4 || soFar == isbns.length )
				carousel.classList.remove('bookCarouselLoading');

			if( typeof resp.imgurl == 'undefined' || ! resp.imgurl )
				return;

			var toAppend;
			var img = document.createElement('img')
			img.setAttribute('src', resp.imgurl);
			img.setAttribute('alt', 'ISBN: ' + resp.isbn);

			if( typeof book_carousel_obj.catLink !== 'undefined' && resp.isbn ){
				var a = document.createElement('a');
				var catLink = book_carousel_obj.catLink.replace('%s', resp.isbn);
				a.setAttribute('href', catLink);
				a.setAttribute('target', 'bookLookup');
				
				a.appendChild(img);
				toAppend = a;
			} else {
				toAppend = img;
			}

			carousel.querySelector('#bookCarousel-inner').appendChild(toAppend);

		});
	}

});