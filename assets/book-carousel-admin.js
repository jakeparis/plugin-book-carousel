jQuery(function($){

	$(".insertNewRow").click(function(){
		var tr = $(this).parents('tr:first');
		var ntr = tr.clone( true );
		ntr.find('img').remove();
		ntr.find('input[type="text"]').val('');
		ntr.addClass('newInputRow');
		tr.after(ntr);
		return false;
	});

	$('.deleteRow').click(function(){
		$(this).parents('tr:first').remove();
		return false;
	});

	$('.js-book_carousel-reset-background').on('click',function(e){
		e.preventDefault();

		$('#book_carousel-config-background-input').val('');
		var $preview = $(this).parents('form').find('.background-preview');
		$preview.attr('style', $preview.data('default') );

	});


	var setPreviewBackground = function(newVal) {
		var $preview = $('.background-preview');
		if( newVal.indexOf('http') === 0) {
			$preview.css({
				"background-image": 'url('+newVal+')',
				"background-color": ''
			});
		} else {
			$preview.css({
				"background-color": newVal,
				"background-image": ''
			});
		}
	};

	$('#iris-color-picker').iris({
		change: function(event,ui) {
			setPreviewBackground( ui.color.toString() );
			$('#book_carousel-config-background-input').val( ui.color.toString() );
		}
	});


	$('#book_carousel-config-background-input').on('keyup',function() {

		var newVal = $(this).val().toLowerCase();
		if(newVal.length < 3) return;

		setPreviewBackground(newVal);

	});


	$('.js-show-colorchooser').on('click',function(e){
		e.preventDefault();
		$('#iris-color-picker').iris('show');
	});

});