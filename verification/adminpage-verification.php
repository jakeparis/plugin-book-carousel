<?php
defined('ABSPATH') || die('Not allowed');
?>

<div class="wrap book_carousel-settings-page">

	<h1>Book Carousel Settings</h1>

	<?php
	if( isset($_POST['submit-verification']) ){
		if( ! wp_verify_nonce( $_POST['_jp-license-nonce'], 'jp-license-update' ) ){
			echo '<div class="updated error"><p>There was a problem. Perhaps the form timed out.</p></div>';
		} else {

			if( ! $_POST['jp-license'] ) {
				echo '<div class="updated error"><p>Please enter a license.</p></div>';
			} else {
				$result = JP_BC_Verification::verifyLicense( $_POST['jp-license'] );

				if( ! $result ){
					echo '<div class="updated error"><p>That license is not valid.</p></div>';
				} else {
					echo '<div class="updated"><p>Thank you! The plugin has been validated. <a href="">Click here to start setting up</a>.</p></div>';

				}
			}
		}
	}	

	$license = JP_BC_Verification::getLicense();

	?>
	<p>In order to use this plugin, you will need to enter a license key. Please <a href="https://jakeparis.com/get-in-touch/">contact us</a> to get one.</p>
	<form method="post" action="">
		<?php wp_nonce_field( 'jp-license-update', '_jp-license-nonce' ); ?>

		<h3>License</h3>
		<input type="text" name="jp-license" value="<?= $license ?>">

		<?php submit_button('Verify','primary','submit-verification') ?>

	</form>

</div>