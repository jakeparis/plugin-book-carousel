<?php
defined('ABSPATH') || die('Not allowed');

class JPBookCarousel {

	var $pluginVersion = '4.2.0';
	private $configs = null;
	var $defaultBackground = '';

	static function instance(){
		static $me = null;
		if( is_null($me) ){
			$me = new JPBookCarousel();
		}
		return $me;
	}

	private function __construct(){
		$this->defaultBackground = plugins_url('assets/bookCarouselBack.jpg',__FILE__);
	}

	function loadConfigs() {
		$this->configs = get_option('bookCarouselConfig');
	}

	function setConfigs($configs){
		$this->configs = $configs;
		update_option('bookCarouselConfig', $configs);
	}

	function getConfig($optName,$default=''){
		if( is_null($this->configs) )
			$this->loadConfigs();
		if( isset($this->configs[$optName]) && ! empty($this->configs[$optName]) ){
			return $this->configs[$optName];
		}
		return $default;
	}


	function getIsbns(){
		return get_option('bookCarouselIsbns', array());
	}
	function setIsbns($isbns){
		if( ! is_array($isbns) )
			$isbns = array($isbns);
		// take away any blank values, and trim each value
		foreach($isbns as $k=>&$v) {
			$v = trim($v);
			if($v == '')
				unset($isbns[$k]);
		}
		update_option('bookCarouselIsbns', $isbns);
	}

	function getBackground(){
		return $this->getConfig('background', $this->defaultBackground);
	}

	function getBackgroundRule(){

		$bg = $this->getBackground();
		
		if( strpos($bg, 'http') === 0 )
			$out = 'background-image:url('.htmlspecialchars($bg).');';
		else
			$out = 'background-color:'.$bg.';';

		return $out;
	}

	private function getThumbCache($isbn){
		return get_transient("book-carousel-cover_{$isbn}");
	}
	private function setThumbCache($isbn, $imgurl, $updateCache=false){
		if( ! $updateCache ){
			$cached = $this->getThumbCache($isbn);
			if( $cached )
				return;
		}
		return set_transient("book-carousel-cover_{$isbn}", $imgurl, WEEK_IN_SECONDS);
	}
	function deleteThumbCache($isbn){
		delete_transient("book-carousel-cover_{$isbn}");
	}

	function getThumbFromIsbn($isbn, $overrideCache=false ){

		// If we set overrideCache=true, then we reach out to a provider
		// for an image. If we get one, we clear the cache and store the
		// new image.
		if( ! $overrideCache ){
			$cached = $this->getThumbCache($isbn);
			if( $cached )
				return $cached;
		}
		$shouldOverwriteCache = true;

		$thumb = '';

		if($this->getConfig('serviceGoogle') == 1 && $this->getConfig('googleBooksAuth') != '') {
			//TRY GOOGLE BOOKS

			$myAuth = $this->getConfig('googleBooksAuth');

			// get a single result (hopefully) using the isbn query
			$url = "https://www.googleapis.com/books/v1/volumes?q={$isbn}&key={$myAuth}&projection=lite&maxResults=1";

			$ch = curl_init($url);
			curl_setopt_array($ch, array(
			    CURLOPT_RETURNTRANSFER => true
			    ,CURLOPT_TIMEOUT => 4 // seconds
			    )
			);
			$json = curl_exec($ch);
			curl_close($ch);

			if( $json ) {
				$obj = json_decode( $json );
				$thumb = $obj->items[0]->volumeInfo->imageLinks->thumbnail;
				$thumb = str_replace('&edge=curl','',$thumb); // turn off curling page effect;
				$this->setThumbCache($isbn, $thumb, $shouldOverwriteCache);
				return $thumb;
			}
		}

		if($this->getConfig('serviceAmazon') == 1 &&
		   $this->getConfig('amazonPublicKey') != '' &&
		   $this->getConfig('amazonPrivateKey') != ''
		   ) {

			// TRY AMAZON
			// modified from http://www.ulrichmierendorff.com/software/aws_hmac_signer.html
			// thank you!
			//
			// Oct 2013 Amazon says they are killing the afilliate program in Maine :(

			$public_key = $this->getConfig('amazonPublicKey');
			$private_key = $this->getConfig('amazonPrivateKey');

			$method = 'GET';
			$host = 'webservices.amazon.com';
			$uri = '/onca/xml';

			$params = array(
				'Keywords'=>$isbn,
				'Operation'=>'ItemSearch',
				'SearchIndex'=>'Books',
				'ResponseGroup'=>'Images'
				);

			$params['Service'] = 'AWSECommerceService';
			$params['AWSAccessKeyId'] = $this->getConfig('amazonPublicKey');
			$params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
			$params['Version'] = '2011-08-01';
			$params['AssociateTag'] = $this->getConfig('amazonAssociateTag');


			// sort the parameters
			ksort($params);

			// create the canonicalized query
			$canonicalized_query = array();
			foreach ($params as $param=>$value)
			{
				$param = str_replace('%7E', '~', rawurlencode($param));
				$value = str_replace('%7E', '~', rawurlencode($value));
				$canonicalized_query[] = $param.'='.$value;
			}
			$canonicalized_query = implode('&', $canonicalized_query);

			// create the string to sign
			$string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;

			// calculate HMAC with SHA256 and base64-encoding
			$signature = base64_encode(hash_hmac('sha256', $string_to_sign, $this->getConfig('amazonPrivateKey'), TRUE));

			// encode the signature for the request
			$signature = str_replace('%7E', '~', rawurlencode($signature));

			// create request
			$request = 'http://'.$host.$uri.'?'.$canonicalized_query.'&Signature='.$signature;

			$res = simplexml_load_file($request);

			if($res) {
				$thumb = $res->Items->Item->MediumImage->URL;
				if($thumb != ''){
					$this->setThumbCache($isbn, $thumb, $shouldOverwriteCache);
					return $thumb;
				}
			}
		}

		if($this->getConfig('serviceCustom') == 1) {
			// TRY SYNDETICS

			$imgUrl = sprintf($this->getConfig('customUrl'), $isbn);
			list($w) = getimagesize($imgUrl);
			if($w > 2) {
				$this->setThumbCache($isbn, $imgUrl, $shouldOverwriteCache);
				return $imgUrl;
			}
		}

		if($this->getConfig('serviceOpenlibrary') == 1) {
			// TRY OPENLIBRARY
			$url = 'http://covers.openlibrary.org/b/isbn/'.$isbn.'-M.jpg';
			list($w) = getimagesize($url);

			if($w > 1) {
				$this->setThumbCache($isbn, $url, $shouldOverwriteCache);
				return $url;
			}
		}

		$defaultUrl = plugins_url('assets/noCover.jpg', __FILE__);
		$this->setThumbCache($isbn, $defaultUrl);
		return $defaultUrl;

	}

}