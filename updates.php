<?php
defined('ABSPATH') || die('Not allowed');

$storedVersion = get_option("book-carousel-plugin-version", 0);


// bail if we're at the newest version
if( version_compare( 
		JP_BOOK_CAROUSEL_PLUGIN_VERSION,
		$storedVersion
	) < 1
) {
	return;
}


/* Updates for various plugin versions. 
 *
 * Use the new version number in the === compare 
 */

// pre-5.0.0 hadn't been storing version numbers
if( $storedVersion == 0 ) {

}

// if( JP_BOOK_CAROUSEL_PLUGIN_VERSION == '5.0.0' ){
	
// }


/* After running any needed updates, store the latest plugin version so we're
 * all up to date again.
 */
update_option("book-carousel-plugin-version", JP_BOOK_CAROUSEL_PLUGIN_VERSION);
