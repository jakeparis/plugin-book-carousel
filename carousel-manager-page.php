<?php
defined('ABSPATH') || die('Not allowed');

echo '<div class="wrap">
<h1>Book Carousel Manager</h1>
<p>Book cover images are cached for one week to display them faster. When you save on this page, you will refresh those cached images.</p>
';

$bc = JPBookCarousel::instance();

if( isset($_POST['ISBNsubmit']) ) {

	if( ! wp_verify_nonce( $_POST['_book-carousel-settings-nonce'], '_update-book-carousel-settings' ) ){
		echo '<div class="updated error"><p>Error. Perhaps the form timed out?</p></div>';
	} else {

		if($_POST['isbnPaste'] != '' && $_POST['isbnPaste'] != ' ') { // if they used the textarea
			$isbns = trim($_POST['isbnPaste']);
			$isbns = explode("\n",$isbns);
		} else {
			$isbns = $_POST['isbn'];
		}

		$bc->setIsbns( $isbns );

		echo '<div class="updated fade"><p>Updated Book Carousel</p></div>';
	}

} // end posted data

$isbns = $bc->getIsbns();
?>

<form class="optionsForm" action="" name="updateInfo" method="post" id="updateInfo">

	<?php wp_nonce_field( "_update-book-carousel-settings", '_book-carousel-settings-nonce' ); ?>
	<table style="width: 45%;float:left;">
		<?php

		foreach($isbns as $isbn) {
			echo '<tr>
				<td><img style="height: 100px;" src="' . $bc->getThumbFromIsbn($isbn, true) . '" alt="book cover" /></td>
				<td><input type="text" name="isbn[]" value="'.$isbn.'" /></td>
				<td> <a href="#" class="insertNewRow">insert row</a> &nbsp; | &nbsp; <a href="#" class="deleteRow">delete this row</a></td>
				  </tr>';
		}
		// plus add 5 more at the end;
		for($c = 1; $c <= 5 ; $c++) {
			echo '<tr>
				<td>&nbsp;</td>
				<td><input type="text" name="isbn[]" value="" /></td>
				<td> <a href="#" class="insertNewRow">insert row</a> &nbsp; | &nbsp; <a href="#" class="deleteRow">delete this row</a></td>
			</tr>';
		}
		?>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="ISBNsubmit" value="Save" class="button-primary"></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<p style="margin:0px;">...Or paste in a list of ISBNs, one on each line</p>
	<textarea name="isbnPaste" style="width: 30%;height: 170px;"></textarea>
	<br>
	<input type="submit" name="ISBNsubmit" value="Save" class="button-primary">
</form>

