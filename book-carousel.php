<?php
/*
Plugin Name: Book Carousel
Description: Adds book carousel functionality. Especially setup for libraries.
Version: 5.2.1
Author: Jake Paris
Author URI: https://jakeparis.com/
*/

define('JP_BOOK_CAROUSEL_PLUGIN_VERSION', '5.2.1');
define('JP_BOOK_CAROUSEL_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define('JP_BOOK_CAROUSEL_PLUGIN_URL', plugins_url('/',__FILE__) );

add_action('admin_init',function(){
	require_once JP_BOOK_CAROUSEL_PLUGIN_PATH . 'updates.php';
});

require_once JP_BOOK_CAROUSEL_PLUGIN_PATH . 'verification/verify.php';


require_once JP_BOOK_CAROUSEL_PLUGIN_PATH . 'JPBookCarousel.class.php';

add_action('admin_menu', function(){
	if( JP_BC_Verification::isFullyVerified(true) ) {
		add_submenu_page('upload.php',  'Book Carousel', 'Book Carousel', 'edit_posts', 'book-carousel-manager-page','book_carousel_manager_page');
	}
	add_submenu_page('options-general.php',  'Book Carousel Settings', 'Book Carousel', 'edit_posts', 'book-carousel-settings','book_carousel_settings_page');
});

add_filter("plugin_action_links_".plugin_basename(__FILE__), function($links){
	$links[] = '<a href="options-general.php?page=book-carousel-settings">Settings</a>';
	return $links;
});

add_action('admin_enqueue_scripts', function(){
	wp_enqueue_script( 'book-carousel-admin', 
		JP_BOOK_CAROUSEL_PLUGIN_URL . 'assets/book-carousel-admin.js', 
		array('jquery','iris'), 
		JP_BOOK_CAROUSEL_PLUGIN_VERSION 
	);	
	wp_enqueue_style( 'book-carousel-admin', 
		JP_BOOK_CAROUSEL_PLUGIN_URL . 'assets/book-carousel-admin.css', 
		array(), 
		JP_BOOK_CAROUSEL_PLUGIN_VERSION 
	);
});

add_action('wp_enqueue_scripts', function() {

	wp_enqueue_style( 'book-carousel',
		JP_BOOK_CAROUSEL_PLUGIN_URL . 'assets/book-carousel.css',
		array(),
		JP_BOOK_CAROUSEL_PLUGIN_VERSION
	);
	wp_enqueue_script( 'book-carousel-js',
		JP_BOOK_CAROUSEL_PLUGIN_URL . 'assets/book-carousel.js',
		array('jquery'),
		JP_BOOK_CAROUSEL_PLUGIN_VERSION
	);

	if( ! empty($_SERVER['HTTPS']) )
		$args = array('ajaxurl' => admin_url( 'admin-ajax.php','https' ) );
	else
		$args = array('ajaxurl' => admin_url( 'admin-ajax.php','http' ) );

	$bc = JPBookCarousel::instance();
	$args['catLink'] = $bc->getConfig('catalogLink');
	$args['isbns'] = $bc->getIsbns();

	wp_localize_script( 'book-carousel-js', 'book_carousel_obj', $args );
});


function book_carousel_settings_page(){
	if( JP_BC_Verification::isFullyVerified(true) )
		require JP_BOOK_CAROUSEL_PLUGIN_PATH . 'admin-settings.php';
	else
		require JP_BOOK_CAROUSEL_PLUGIN_PATH . 'verification/adminpage-verification.php';
}

function book_carousel_manager_page(){
	require JP_BOOK_CAROUSEL_PLUGIN_PATH . 'carousel-manager-page.php';
}


add_shortcode( 'book_carousel', 'book_carousel_show');
function book_carousel_show() {

	$bc = JPBookCarousel::instance();

	$seeMoreUrl = $bc->getConfig('seeMoreUrl');
	if( $seeMoreUrl )
		$seeMoreLink = '<p id="seeMoreCarousel"><a href="'.esc_attr($seeMoreUrl).'">See more</a></p>';

	$out = '<div id="bookCarousel" style="'.esc_attr($bc->getBackgroundRule()).'">
		'.$seeMoreLink.'
		<div id="bookCarousel-inner">
			<img id="bookFiller" alt="book filler" src="' . plugins_url('assets/bookFiller.gif',__FILE__) . '">
		</div>
	</div>';

	return $out;

}


add_action('wp_ajax_getBookCover','book_carousel_getBookCover');
add_action('wp_ajax_nopriv_getBookCover','book_carousel_getBookCover');

function book_carousel_getBookCover(){
	header("Content-Type: application/json");
	$isbn = $_POST['isbn'];
	$bc = JPBookCarousel::instance();
	$imgurl = $bc->getThumbFromIsbn($isbn);
	echo json_encode(array(
		'imgurl' => $imgurl,
		'isbn' => $isbn
	));
	exit;
}

/**
 * Updater
 */
require plugin_dir_path(__FILE__) . 'updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/plugin-book-carousel',
	__FILE__, //Full path to the main plugin file or functions.php.
	'book-carousel/book-carousel.php'
);


register_deactivation_hook( __FILE__, function(){
	JP_BC_Verification::setKey(false);
});
