<?php
defined('ABSPATH') || die('Not allowed');

echo '<div class="wrap book_carousel-settings-page">
	<h1>Book Carousel Settings</h1>';

$bc = JPBookCarousel::instance();

if( isset($_POST['configSubmit']) ) {
	$config = $_POST['config'];

	if ( ! wp_verify_nonce( $_POST['my_sbcc_nonce'], 'save_book_carousel_configs' ) ){
		echo '<div class="updated error">Error. Perhaps the form expired. Please try again. </div>';

	} else if ( isset($config['serviceGoogle']) && $config['googleBooksAuth'] == '') {
		echo '<div class="updated error">You have to enter an API auth code in order to use the Google Books service.</div>';

	} else if( isset($config['serviceAmazon']) && (
		$config['amazonAssociateTag'] == '' 
		|| $config['amazonPublicKey'] == '' 
		|| $config['amazonPrivateKey'] == '' 
		)
	) {
		echo '<div class="updated error">You have to fill in the complete Amazon information in order to use the Amazon service.</div>';

	} else {

		if( isset($config['background']) && $config['background'] != '' )
			$config['background'] = htmlspecialchars($config['background']);

		$bc->setConfigs($config);
		$bc->loadConfigs();
		echo '<div class="updated fade">Updated Book Carousel Settings</div>';
	}

} // end posted data

?>

<p>
	<b>Usage</b>
	<br>
	Using the page/post editor, type this shortcode where you want the carousel to show up: <code>[book_carousel]</code><br>
</p>
<hr>
<form class="optionsForm" action="" name="updateInfo" method="post" id="updateInfo">

	<?php wp_nonce_field('save_book_carousel_configs','my_sbcc_nonce') ?>

	<h2>General Options</h2>
	<table>
		<tr>
			<td>"See More" link goes to: </td>
			<td><input  style="width:300px;" type="text" value="<?php echo $bc->getConfig('seeMoreUrl');?>" name="config[seeMoreUrl]"></td>
		</tr>
		<tr>
			<td>Catalog Link structure</td>
			<td><input style="width:300px;" type="text" value="<?php echo $bc->getConfig('catalogLink');?>" name="config[catalogLink]"><br>
		<span class="incidental">Use <b>%s</b> in place of the isbn, like</span> <code>https://minerva.maine.edu/search~S19/i<b>%s</b></code></td>
		</tr>
		<tr>
			<td>Widget Background</td>
			<td>
				<div id="iris-color-picker"></div>
				<span class="incidental">Enter an image url or a hex color here to override the default.</span><br>

				<input type="text" 
					value="<?php echo $bc->getConfig('background','') ?>" 
					name="config[background]" 
					id="book_carousel-config-background-input"
				>
				<a href="#" class="js-show-colorchooser">Pick a color</a>
				<br>
			    <div class="background-preview" 
			    	data-default="background-image:url(<?= $bc->defaultBackground ?>)" 
			    	style="<?php echo $bc->getBackgroundRule() ?>"
			    >
			    </div>

				<a href="#" class="js-book_carousel-reset-background">reset to default</a>
				<br>
			</td>
		</tr>
	</table>

	<hr>

	<h2>Book Cover Services</h2>
	<p>The carousel uses the isbns you enter to grab book covers from the service(s) of your choice. Configuring multiple services below is a good idea. For instance, if you max out the covers allowed by the first cover service, the carousel will automatically start loading them from the next, and so on.
	</p>
	
	<table>
		<tr>
			<th style="text-align:left;">Use This?</th>
			<th style="text-align:center;">Service Name</th>
			<th style="text-align:left;">Service Options</th>
		</tr>
		<tr>
			<td>
				<input type="checkbox" value="1" name="config[serviceGoogle]" <?php if($bc->getConfig('serviceGoogle') == 1) echo 'checked="checked"';?>>
			</td>
			<td>
				<b>Google Books</b>
			</td>
			<td>Google Books Authorization Key
				<input  style="width:300px;" type="text" name="config[googleBooksAuth]" value="<?php echo $bc->getConfig('googleBooksAuth') ?>">
				<p class="incidental">You can get one of these at <a href="https://code.google.com/apis/console" target="_blank">https://code.google.com/apis/console</a></p>
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" value="1" name="config[serviceAmazon]" <?php if($bc->getConfig('serviceAmazon') == 1) echo 'checked="checked"';?>>
			</td>
			<td>
				<b>Amazon</b>
			</td>
			<td>
				Associate Tag <input  style="width:300px;" type="text" value="<?php echo $bc->getConfig('amazonAssociateTag') ?>" name="config[amazonAssociateTag]"><br>
				Public Key <input  style="width:300px;" type="text" value="<?php echo $bc->getConfig('amazonPublicKey') ?>" name="config[amazonPublicKey]"><br>
				Private Key <input  style="width:300px;" type="text" value="<?php echo $bc->getConfig('amazonPrivateKey') ?>" name="config[amazonPrivateKey]">
				<p class="incidental">You can get this stuff by <a href="https://affiliate-program.amazon.com/" target="_blank">creating an Amazon Associates account</a>, then going to <a href="https://portal.aws.amazon.com/gp/aws/securityCredentials" target="_blank">https://portal.aws.amazon.com/gp/aws/securityCredentials</a>.</p>
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" value="1" name="config[serviceOpenlibrary]" <?php if($bc->getConfig('serviceOpenlibrary') == 1) echo 'checked="checked"';?>>
			</td>
			<td>
				<b>Open Library</b></td>
			<td>
				No configuration needed. Coverage isn't as broad as some of the others.
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" value="1" name="config[serviceCustom]" <?php if($bc->getConfig('serviceCustom') == 1) echo 'checked="checked"';?>>
			</td>
			<td>
				<b>Custom book cover service</b><br>
				<span class="incidental">Like, for example, Syndetics</span>
			</td>
			<td>
				<input  style="width:300px;" type="text" value="<?php echo $bc->getConfig('customUrl') ?>" name="config[customUrl]"><br>
				<span class="incidental">Use <b>%s</b> in place of the isbn, like</span>
				<?php //<code>http://syndetics.com/index.aspx?isbn=<b>%s</b>/sc.gif</code>
				?>
				<code>http://example-book-covers.com/?isbn=<b>%s</b>/cover.jpg</code>
			</td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>
				<input type="submit" name="configSubmit" value="Save" class="button-primary">
			</td>
		</tr>
	</table>

</form>
